//import dependencies

const User = require("../models/user")
const bcrypt = require("bcrypt")// is used to encrypt our password
// encryption is a way to hide your data
// e.g. when logging into Facebook, you give your password as "password123"
// The value in the database is not stored as "password123" but instead, a random
// set of letters and numbers (e.g. iwo654456asdcklmask)
const auth = require("../auth")// import auth.js file to use its authorization functions

module.exports.checkEmail = (body) =>{
	return User.find({email: body.email}).then(result =>{
		if(result.length>0){
			return true // duplicate email found
		}else{
			return false // email not yet registered
		}
	})	
}

// if a duplicate is above, it will return the following data:
/*
	[
		{
			email: "duplicate@email.com"(example)
		}
	]
*/

// if no duplicate is found, it returns the following data
// []

module.exports.registerUser = (body) =>{
	let newUser = new User({
		firstName:body.firstName,
		lastName:body.lastName,
		email:body.email,
		mobileNo:body.mobileNo,
		//passwords are Never saved in plain text, hence we will use bcrypt's
		//hashSync function to encrypt our users' passwords
		password:bcrypt.hashSync(body.password, 10)
		// 10 in this value refers to a concept called "salt", which is the number of times
		// the encryption is run on the password
	})

	return newUser.save().then((user,error) =>{
		if(error){
			return false; // user was NOT saved
		}else {
			return true; // user was successfully saved
		}
	})
}

module.exports.loginUser = (body) =>{
	//findOne is a mongoose alternative to find() that ensures that only the first result
	// is obtained
	return User.findOne({email: body.email}).then(result=>{
		if(result === null){
			return false // user does not exist
		}else{
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)

			//compareSync() is used to compare a non-encrypted password to the encrypted
			//password in our database. If there is a match, it returns true. Otherwise,
			//it returns false.

			if(isPasswordCorrect){
				//if isPasswordCorrect is true, passwords match so an access token
				// is created and we can grant it to our user
				return{access: auth.createAccessToken(result.toObject())}
			}else{
				return false; //meaning passwords do not match
			}
		}
	})
}





